<?php

use Illuminate\Database\Seeder;

class SitesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $site = new \App\Entities\Site([
            'name' => 'votum',
            'title' => 'Votum Website'
        ]);

        $site->save();
    }
}
