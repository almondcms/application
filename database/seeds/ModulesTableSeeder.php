<?php

use Illuminate\Database\Seeder;

class ModulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pageModule = new \App\Entities\Module([
            'name' => 'Pages Module',
            'namespace' => \App\Modules\Pages\ModuleServiceProvider::class,
            'description' => '123',
            'is_system' => true
        ]);

        $pageModule->save();
    }
}
