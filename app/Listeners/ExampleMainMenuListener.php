<?php

namespace App\Listeners;

use App\Events\Admin\Components\FetchMainMenuItemsEvent;
use App\Http\Components\Menu\MenuItem;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ExampleMainMenuListener
{
    /**
     * Handle the event.
     *
     * @param  FetchMainMenuItemsEvent  $event
     * @return void
     */
    public function handle(FetchMainMenuItemsEvent $event)
    {
        $menuCollection = $event->getMenuCollector();

        $group = $menuCollection->get('site::management');

        $group->add(new MenuItem('Create Website', '\\App\\Http\\Controllers\\Admin\\SitesController@getCreate', [
            'icon' => 'icon-grid'
        ]));
    }
}
