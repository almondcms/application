<?php

namespace App\Http\Components\Menu\Interfaces;

interface MenuCollectorContract
{
    public function add(MenuGroupContract $menuGroup);

    /**
     * @param string $menuId
     *
     * @return MenuGroupContract
     */
    public function get($menuId);
    public function getMenus();
}