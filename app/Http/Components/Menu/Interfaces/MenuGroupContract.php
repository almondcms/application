<?php

namespace App\Http\Components\Menu\Interfaces;

interface MenuGroupContract
{
    public function render();
    public function getUniqueIdentifier();
    public function getLabel();

    /**
     * Adds a new Menu Item
     *
     * @param MenuItemContract $menuItem
     *
     * @return MenuGroupContract
     */
    public function add(MenuItemContract $menuItem);

    /**
     * @return array
     */
    public function getMenus();

    /**
     * @return bool
     */
    public function hasMenus();
}