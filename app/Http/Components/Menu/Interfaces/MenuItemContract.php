<?php

namespace App\Http\Components\Menu\Interfaces;


interface MenuItemContract
{
    public function render();
    
    public function getLabel();
    public function getUrl();
    public function getIsActive();
}