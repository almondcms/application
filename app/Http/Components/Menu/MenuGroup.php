<?php

namespace App\Http\Components\Menu;

use App\Http\Components\Menu\Interfaces\MenuGroupContract;
use App\Http\Components\Menu\Interfaces\MenuItemContract;

class MenuGroup implements MenuGroupContract
{
    protected $label;
    protected $uniqueIdentifier;
    protected $menus = [];

    public function __construct($label, $uniqueIdentifier = null)
    {
        $this->label = $label;
        $this->uniqueIdentifier = $uniqueIdentifier;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function render()
    {
        $label = $this->getLabel();
        $menus = $this->getMenus();
        $hasMenus = $this->hasMenus();

        return view('components.menu.group', compact('label', 'menus', 'hasMenus'));
    }

    public function getUniqueIdentifier()
    {
        return $this->uniqueIdentifier;
    }

    public function add(MenuItemContract $menuItem)
    {
        $this->menus[] = $menuItem;
        return $this;
    }

    public function getMenus()
    {
        return $this->menus;
    }

    public function hasMenus()
    {
        return count($this->getMenus()) > 0;
    }


}