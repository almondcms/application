<?php

namespace App\Http\Components\Menu;

use App\Http\Components\Menu\Interfaces\MenuItemContract;
use Illuminate\Routing\Route;

class MenuItem implements MenuItemContract
{
    protected $label;
    protected $action;
    protected $options = [];

    public function __construct($label, $action, $options = [])
    {
        $this->label = $label;
        $this->action = $action;
        $this->options = $options;
    }

    public function render()
    {
        $label = $this->getLabel();
        $isActive = $this->getIsActive();
        $url = $this->getUrl();
        $options = $this->getOptions();

        return view('components.menu.item', compact('label', 'url', 'isActive', 'options'));
    }

    public function getLabel()
    {
        return $this->label;
    }

    protected function getOptions()
    {
        return $this->options;
    }

    public function getUrl()
    {
        return action($this->action);
    }

    public function getIsActive()
    {
        return $this->getUrl() === url(\Route::current()->getPath());
    }


}