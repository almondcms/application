<?php

namespace App\Http\Components\Menu;

use App\Http\Components\Menu\Interfaces\MenuCollectorContract;
use App\Http\Components\Menu\Interfaces\MenuGroupContract;

class MenuCollector implements MenuCollectorContract
{
    protected $menuGroups = [];

    /**
     * Adds a new Menu Group to the Menus
     *
     * @param MenuGroupContract $menuGroup
     *
     * @return MenuGroupContract
     */
    public function add(MenuGroupContract $menuGroup)
    {
        $this->menuGroups[$menuGroup->getUniqueIdentifier()] = $menuGroup;

        return $this->get($menuGroup->getUniqueIdentifier());
    }

    /**
     * @param string $menuId
     * 
     * @return MenuGroupContract
     */
    public function get($menuId)
    {
        return $this->menuGroups[$menuId];
    }

    public function getMenus()
    {
        return $this->menuGroups;
    }


}