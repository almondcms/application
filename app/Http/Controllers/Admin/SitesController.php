<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Module;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SitesController extends Controller
{
    public function getCreate()
    {
        $modules = Module::all();
        return view('admin.sites.create', compact('modules'));
    }
}
