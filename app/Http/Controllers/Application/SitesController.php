<?php

namespace App\Http\Controllers\Application;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SitesController extends Controller
{
    public function getCreate(Request $request)
    {
        $site = $request->query('site', '');
        $suffix = '.website.'.env('APP_DOMAIN', 'almondcms.dev');

        if(\Auth::check()) {
            return redirect()->route('create-website-auth', compact('site'));
        }
        

        return view('sites.create', compact('site', 'suffix'));
    }
}
