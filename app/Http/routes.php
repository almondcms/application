<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

$modules = App\Entities\Module::where('is_system', true)->get();

foreach($modules as $module)
{
    App::register($module->namespace);
}

Route::group([
    'domain' => 'cms.' . env('APP_DOMAIN', 'almondcms.dev'),
], function() {

    Route::auth();

    Route::group(['namespace' => 'Application'], function() {
        Route::get('/', function () {
            return "teste";
        });

        Route::get('/create', 'SitesController@getCreate')->name('create-website');

    });

    Route::group([
        'namespace' => 'Admin',
        'middleware' => 'auth'
    ], function() {

        Route::get('/', function() {
            return 'admin';
        });

        Route::get('/sites/create', 'SitesController@getCreate')->name('create-website-auth');;
    });

    Route::get('{slug?}', function($slug) {
        abort(404);
    })->where('slug', '.*');
});


Route::group(['middleware' => 'check-domain'], function() {

    Route::get('/', function () {
        $site = app('current-site');

        return view('welcome', compact('site'));
    });

    Route::get('/home', 'HomeController@index');

    Route::get('{slug?}', function($slug) {

        $site = app('current-site');

        $page = $site->pages()->whereSlug($slug)->first();

        if($page === null)
        {
            abort(404);
        }

        return $page->title;

    })->where('slug', '.*');

});