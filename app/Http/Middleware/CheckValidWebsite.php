<?php

namespace App\Http\Middleware;

use App\Entities\Site;
use Closure;

class CheckValidWebsite
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $name = $this->getWebsiteName($request->getHost());

        // Parse the Domain Website

        // Verify if exists

        // If Not

        // Is it a name without dots?



        // ola.website.almondcms.dev
        // $name = "ola"
        //
        // votum.de -> votum.website.almondcms.dev
        // $name = "votum";

        if($this->isValidWebsite($name)) {

            \App::singleton('current-site', function($app) use($name) {
                return Site::whereName($name)->first();
            });

            return $next($request);
        } elseif($this->domainIsAName($name)) {
            return redirect()->route('create-website', ['site' => $name]);
        }

        // TODO: Change to a user friendly page
        abort(404);
        return;
    }

    private function isValidWebsite($name)
    {
        return Site::whereName($name)->count() > 0;
    }

    private function getWebsiteName($domain)
    {
        $result = strpos($domain, '.website.'.env('APP_DOMAIN','almondcms.dev'));

        if($result) {
            return substr($domain, 0, $result);
        }

        return $domain;
    }

    private function domainIsAName($domain) {
        return strpos($domain, '.') ? false : true;
    }
}
