<?php

namespace App\Http\ViewComposers\Layout;

use App\Events\Admin\Components\FetchMainMenuItemsEvent;
use App\Http\Components\Menu\MenuCollector;
use App\Http\Components\Menu\MenuGroup;
use App\Http\Components\Menu\MenuItem;
use App\Http\ViewComposers\ViewComposer;
use Illuminate\View\View;

class AsideMenuComposer extends ViewComposer
{
    public function compose(View $view)
    {
        $menuCollection = new MenuCollector();

        $menuCollection->add(new MenuGroup('Website Management', 'site::management'));

        event(new FetchMainMenuItemsEvent($menuCollection));

        $view->with('menus', $menuCollection->getMenus());
    }

}