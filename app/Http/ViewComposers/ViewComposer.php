<?php
/**
 * Created by PhpStorm.
 * User: jorgemurta
 * Date: 23/08/16
 * Time: 09:28
 */

namespace App\Http\ViewComposers;


use Illuminate\View\View;

abstract class ViewComposer
{
    public abstract function compose(View $view);
}