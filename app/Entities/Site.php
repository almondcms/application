<?php

namespace App\Entities;

use Alsofronie\Uuid\UuidModelTrait;
use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    use UuidModelTrait;
    
    protected $fillable = [
        'name', 'title'
    ];

    public function pages()
    {
        return $this->hasMany(Page::class);
    }
}
