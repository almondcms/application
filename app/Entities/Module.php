<?php

namespace App\Entities;

use Alsofronie\Uuid\UuidModelTrait;
use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    use UuidModelTrait;
    
    protected $fillable = [
        'name', 'namespace', 'description', 'is_system'
    ];
}