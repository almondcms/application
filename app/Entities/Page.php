<?php

namespace App\Entities;

use Alsofronie\Uuid\UuidModelTrait;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use UuidModelTrait;

    public function site()
    {
        return $this->belongsTo(Site::class);
    }
}
