<?php

namespace App\Events\Admin\Components;

use App\Events\Event;
use App\Http\Components\Menu\Interfaces\MenuCollectorContract;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class FetchMainMenuItemsEvent extends Event
{
    use SerializesModels;

    /**
     * @var MenuCollectorContract
     */
    protected $menuCollector;

    /**
     * Create a new event instance.
     *
     * @param MenuCollectorContract $menuCollector
     */
    public function __construct(MenuCollectorContract $menuCollector)
    {
        $this->menuCollector = $menuCollector;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }

    public function getMenuCollector()
    {
        return $this->menuCollector;
    }
}
