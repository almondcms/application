<?php

namespace App\Modules\Pages\Listeners;

use App\Events\Admin\Components\FetchMainMenuItemsEvent;
use App\Http\Components\Menu\MenuGroup;
use App\Http\Components\Menu\MenuItem;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class MainMenuListener
{
    /**
     * Handle the event.
     *
     * @param  FetchMainMenuItemsEvent  $event
     * @return void
     */
    public function handle(FetchMainMenuItemsEvent $event)
    {
        $menuCollection = $event->getMenuCollector();

        $group = $menuCollection->get('site::management');

        $group->add(new MenuItem('Page Manager', '\\App\\Http\\Controllers\\Admin\\SitesController@getCreate', [
            'icon' => 'icon-grid'
        ]));

        $groupPages = $menuCollection->add(new MenuGroup('Ola daqui'));

        $groupPages->add(new MenuItem('Page Manager', '\\App\\Http\\Controllers\\Admin\\SitesController@getCreate', [
            'icon' => 'icon-grid'
        ]));
    }
}
