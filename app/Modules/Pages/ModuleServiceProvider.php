<?php

namespace App\Modules\Pages;

use App\Modules\Base\BaseModuleServiceProvider;
use App\Modules\Pages\Providers\EventServiceProvider;

class ModuleServiceProvider extends BaseModuleServiceProvider
{
    public function getModuleServiceProviders()
    {
        return [
            EventServiceProvider::class
        ];
    }

}