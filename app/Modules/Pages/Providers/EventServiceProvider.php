<?php

namespace App\Modules\Pages\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;


class EventServiceProvider extends ServiceProvider 
{
    protected $listen = [
        'App\Events\Admin\Components\FetchMainMenuItemsEvent' => [
            'App\Modules\Pages\Listeners\MainMenuListener',
        ],
    ];
}