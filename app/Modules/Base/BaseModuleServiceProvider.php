<?php

namespace App\Modules\Base;

use App\Events\Admin\Components\FetchMainMenuItemsEvent;
use App\Http\Components\Menu\Interfaces\MenuCollectorContract;
use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;

abstract class BaseModuleServiceProvider extends ServiceProvider
{
    public function register()
    {
        foreach($this->getModuleServiceProviders() as $serviceProvider)
        {
            $this->app->register($serviceProvider);
        }
    }

    public function boot()
    {

    }

    public function getModuleServiceProviders()
    {
        return [];
    }
}