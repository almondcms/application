@extends('auth.base')

@section('content')
@parent

    <p class="text-center pv">REGISTER A NEW ACCOUNT.</p>
    <form role="form" data-parsley-validate="" novalidate="" class="mb-lg" method="POST" action="{{ url('/register') }}">
        {{ csrf_field() }}
        <div class="form-group has-feedback">
            <div class="input-group m-b">
                <input id="inputSite" value="{{ old('site', $site) }}" name="site" type="text" placeholder="Enter Site Name" autocomplete="off" required class="form-control">
                <span class="input-group-addon">{{$suffix}}</span>
            </div>
        </div>
        <div class="form-group has-feedback">
            <input id="inputName" value="{{ old('name') }}" name="name" type="text" placeholder="Enter Name" autocomplete="on" required class="form-control">
            <span class="fa fa-user form-control-feedback text-muted"></span>
        </div>
        <div class="form-group has-feedback">
            <input id="inputEmail" value="{{ old('email') }}" name="email" type="email" placeholder="Enter email" autocomplete="off" required class="form-control">
            <span class="fa fa-envelope form-control-feedback text-muted"></span>
        </div>
        <div class="form-group has-feedback">
            <input id="exampleInputPassword1" name="password" type="password" placeholder="Password" required class="form-control">
            <span class="fa fa-lock form-control-feedback text-muted"></span>
        </div>
        <div class="form-group has-feedback">
            <input id="exampleInputPasswordConfirm1" name="password_confirmation" type="password" placeholder="Confirm Password" required class="form-control">
            <span class="fa fa-lock form-control-feedback text-muted"></span>
        </div>
        <div class="clearfix">
        </div>
        <button type="submit" class="btn btn-block btn-primary mt-lg">Create Account and Site</button>
    </form>
    <p class="pt-lg text-center">Already have an Account?</p><a href="{{url('/login')}}" class="btn btn-block btn-default">Login with your account</a>
@stop
