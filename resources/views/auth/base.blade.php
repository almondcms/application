@extends('layouts.base')

@section('content')
    <div class="wrapper">
        <div class="block-center mt-xl wd-xl">
            <!-- START panel-->
            <div class="panel panel-dark panel-flat">
                <div class="panel-heading text-center">
                    <a href="#">
                        <img src="{{asset('img/logo.png')}}" alt="Image" class="block-center img-rounded">
                    </a>
                </div>
                <div class="panel-body">
                    @yield('content')
                </div>
            </div>
            <!-- END panel-->
            <div class="p-lg text-center">
                <span>&copy;</span>
                <span>2016</span>
                <span>-</span>
                <span>Angle</span>
                <br>
                <span>Bootstrap Admin Template</span>
            </div>
        </div>
    </div>
@endsection
