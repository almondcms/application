@extends('auth.base')

@section('content')
@parent
<p class="text-center pv">SIGN IN TO CONTINUE.</p>
<form role="form" data-parsley-validate="" novalidate="" class="mb-lg" method="POST" action="{{ url('/login') }}">
    {{ csrf_field() }}
    <div class="form-group has-feedback">
        <input id="inputEmail" value="{{ old('email') }}" name="email" type="email" placeholder="Enter email" autocomplete="off" required class="form-control">
        <span class="fa fa-envelope form-control-feedback text-muted"></span>
    </div>
    <div class="form-group has-feedback">
        <input id="exampleInputPassword1" name="password" type="password" placeholder="Password" required class="form-control">
        <span class="fa fa-lock form-control-feedback text-muted"></span>
    </div>
    <div class="clearfix">
        <div class="checkbox c-checkbox pull-left mt0">
            <label>
                <input type="checkbox" value="{{old('remember')}}" name="remember">
                <span class="fa fa-check"></span>Remember Me</label>
        </div>
        <div class="pull-right"><a href="recover.html" class="text-muted">Forgot your password?</a>
        </div>
    </div>
    <button type="submit" class="btn btn-block btn-primary mt-lg">Login</button>
</form>
<p class="pt-lg text-center">Need to Signup?</p><a href="{{url('/register')}}" class="btn btn-block btn-default">Register Now</a>
@stop
