<li {{array_key_exists('id', $options) ? 'id="'.$options['id'].'"' : ''}} class="{{$isActive ? 'active' : ''}}">
    <a href="{{$url}}" title="{{$label}}">
        @if(array_key_exists('icon', $options))
            <em class="{{$options['icon']}}"></em>
        @endif
        <span>{{$label}}</span>
    </a>
</li>