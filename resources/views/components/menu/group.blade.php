@if($hasMenus)
<li class="nav-heading">
    <span data-localize="sidebar.heading.HEADER">{{$label}}</span>
</li>
@foreach($menus as $menuGroup)
    {!! $menuGroup->render() !!}
@endforeach
@endif