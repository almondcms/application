@extends('layouts.backend')

@section('heading')
    <h3>Create New Website
        <small>You are about to create a new Website and associate it to your Account.</small>
    </h3>
@endsection

@section('content')
@parent
    @foreach($modules as $module)
        {{$module->name}} - {{$module->namespace}}
    @endforeach
@stop