<nav data-sidebar-anyclick-close="" class="sidebar">
    <!-- START sidebar nav-->
    <ul class="nav">
        <!-- START user info-->
        <li class="has-user-block">
            <div id="user-block" class="">
                <div class="item user-block">
                    <!-- User picture-->
                    <div class="user-block-picture">
                        <div class="user-block-status">
                            <img src="img/user/02.jpg" alt="Avatar" width="60" height="60" class="img-thumbnail img-circle">
                            <div class="circle circle-success circle-lg"></div>
                        </div>
                    </div>
                    <!-- Name and Job-->
                    <div class="user-block-info">
                        <span class="user-block-name">Hello, {{\Auth::user()->name}}</span>
                        <span class="user-block-role">Votum - <span style="text-decoration: underline;">change</span></span>
                    </div>
                </div>
            </div>
        </li>


        <!-- END user info-->
        <!-- Iterates over all sidebar items-->
        @foreach($menus as $menuGroup)
            {!! $menuGroup->render() !!}
        @endforeach

    </ul>
    <!-- END sidebar nav-->
</nav>