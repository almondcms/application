<html>
<head>
    <title>AlmondCMS</title>
    <!-- =============== VENDOR STYLES ===============-->
    <!-- FONT AWESOME-->
    <link rel="stylesheet" href="{{asset('vendor/fontawesome/css/font-awesome.min.css')}}">
    <!-- SIMPLE LINE ICONS-->
    <link rel="stylesheet" href="{{asset('vendor/simple-line-icons/css/simple-line-icons.css')}}">
    <!-- ANIMATE.CSS-->
    <link rel="stylesheet" href="{{asset('vendor/animate.css/animate.min.css')}}">
    <!-- WHIRL (spinners)-->
    <link rel="stylesheet" href="{{asset('vendor/whirl/dist/whirl.css')}}">
    <!-- =============== PAGE VENDOR STYLES ===============-->
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
</head>
<body>
@yield('content')

<!-- =============== VENDOR SCRIPTS ===============-->
<!-- MODERNIZR-->
<script src="{{asset('vendor/modernizr/modernizr.custom.js')}}"></script>
<!-- MATCHMEDIA POLYFILL-->
<script src="{{asset('vendor/matchMedia/matchMedia.js')}}"></script>
<!-- JQUERY-->
<script src="{{asset('vendor/jquery/dist/jquery.js')}}"></script>
<!-- BOOTSTRAP-->
<script src="{{asset('vendor/bootstrap/dist/js/bootstrap.js')}}"></script>
<!-- STORAGE API-->
<script src="{{asset('vendor/jQuery-Storage-API/jquery.storageapi.js')}}"></script>
<!-- JQUERY EASING-->
<script src="{{asset('vendor/jquery.easing/js/jquery.easing.js')}}"></script>
<!-- ANIMO-->
<script src="{{asset('vendor/animo.js/animo.js')}}"></script>
<!-- SLIMSCROLL-->
<script src="{{asset('vendor/slimScroll/jquery.slimscroll.min.js')}}"></script>
<!-- SCREENFULL-->
<script src="{{asset('vendor/screenfull/dist/screenfull.js')}}"></script>
<!-- LOCALIZE-->
<script src="{{asset('vendor/jquery-localize-i18n/dist/jquery.localize.js')}}"></script>
<!-- =============== APP SCRIPTS ===============-->
<script src="{{asset('js/app.js')}}"></script>
</body>
</html>